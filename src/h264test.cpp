// h264test.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <cmath>
#include <cstring>

#include <array>
#include <vector>
#include <string>
#include <fstream>
#include <iostream>

#include <codec_api.h>
#include <libyuv.h>

#define MP4V2_NO_STDINT_DEFS
#include <mp4v2/mp4v2.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#if _WIN32
    #define bswap32 _byteswap_ulong
#else
    #define bswap32 __builtin_bswap32
#endif

int main()
{
#if _WIN32
    std::atexit([] { system("pause"); });
#endif

    std::cout << "Hello World!\n";

    if (FILE* fp = fopen("out.mp4", "rb"))
    {
        fclose(fp);

        auto mp4 = MP4Read("trailer.mp4");
        auto mp4_track = MP4FindTrackId(mp4, 0, MP4_VIDEO_TRACK_TYPE);
        printf("INFO:\n%s\n\n", MP4Info(mp4));
        if (mp4_track == MP4_INVALID_TRACK_ID)
        {
            printf("error track\n");
            return 1;
        }

        const int video_width = MP4GetTrackVideoWidth(mp4, mp4_track);
        const int video_height = MP4GetTrackVideoHeight(mp4, mp4_track);

        //decoder declaration
        ISVCDecoder* pSvcDecoder = nullptr;

        //output: [0~2] for Y,U,V buffer for Decoding only
        std::vector<uint8_t> yuv_buffer(video_width * video_height * 3 / 2);
        unsigned char* yuv_planes[3] = {
            std::data(yuv_buffer),
            std::data(yuv_buffer) + video_width * video_height,
            std::data(yuv_buffer) + video_width * video_height + video_width * video_height  / 4,
        };
        
        WelsCreateDecoder(&pSvcDecoder);

        SDecodingParam sDecParam = { 0 };
        sDecParam.sVideoProperty.eVideoBsType = VIDEO_BITSTREAM_AVC;
        //for Parsing only, the assignment is mandatory
        //sDecParam.bParseOnly = true;

        pSvcDecoder->Initialize(&sDecParam);

        // zero terminated arrays
        uint8_t** data_seq = nullptr;
        uint32_t* data_seq_sz = 0;
        uint8_t** data_pic = nullptr;
        uint32_t* data_pic_sz = 0;
        if (!MP4GetTrackH264SeqPictHeaders(mp4, mp4_track, &data_seq, &data_seq_sz, &data_pic, &data_pic_sz))
        {
            printf("cannot read SPS/PPS\n");
            return 1;
        }
        
        //in-out: for Decoding only: declare and initialize the output buffer info, this should never co-exist with Parsing only
        SBufferInfo sDstBufInfo = { 0 };

        // init SPS
        for (int i = 0; data_seq[i] && data_seq_sz[i]; i++)
        {
            std::vector<uint8_t> nal((size_t)data_seq_sz[i] + 4);
            nal[0] = 0; nal[1] = 0; nal[2] = 0; nal[3] = 1;
            std::copy(data_seq[i], data_seq[i] + data_seq_sz[i], &nal[4]);
            if (pSvcDecoder->DecodeFrame2(nal.data(), nal.size(), yuv_planes, &sDstBufInfo) != dsErrorFree)
                printf("error decoding SPS\n");
            MP4Free(data_seq[i]);
        }
        MP4Free(data_seq);
        MP4Free(data_seq_sz);

        // init PPS
        for (int i = 0; data_pic[i] && data_pic_sz[i]; i++)
        {
            std::vector<uint8_t> nal((size_t)data_pic_sz[i] + 4);
            nal[0] = 0; nal[1] = 0; nal[2] = 0; nal[3] = 1;
            std::copy(data_pic[i], data_pic[i] + data_pic_sz[i], &nal[4]);
            if (pSvcDecoder->DecodeFrame2(nal.data(), nal.size(), yuv_planes, &sDstBufInfo) != dsErrorFree)
                printf("error decoding PPS\n");
            MP4Free(data_pic[i]);
        }
        MP4Free(data_pic);
        MP4Free(data_pic_sz);

        // decode loop
        int samples_count = MP4GetTrackNumberOfSamples(mp4, mp4_track);
        int sample_id = 1; // first sample is 1
        int frame = 0;
        while (sample_id <= samples_count)
        {
            uint8_t* data = nullptr;
            uint32_t data_size = 0;
            if (!MP4ReadSample(mp4, mp4_track, sample_id, &data, &data_size))
            {
                printf("error reading sample\n");
                return 1;
            }
            sample_id++;

            //for Decoding only
            for (size_t off = 0; off < data_size;)
            {
                auto ptr = data + off;
                int ssz = bswap32(*reinterpret_cast<uint32_t*>(ptr)) + 4;
                ptr[0] = 0; ptr[1] = 0; ptr[2] = 0; ptr[3] = 1;

                if (pSvcDecoder->DecodeFrameNoDelay(ptr, ssz, yuv_planes, &sDstBufInfo) != dsErrorFree)
                {
                    //error handling (RequestIDR or something like that)
                    printf("decoding error\n");
                    break;
                }
                
                //for Decoding only, pData can be used for render.
                if (sDstBufInfo.iBufferStatus == 1)
                {
                    if (frame == 200)
                    {
                        int w = sDstBufInfo.UsrData.sSystemBuffer.iWidth;
                        int h = sDstBufInfo.UsrData.sSystemBuffer.iHeight;
                        std::vector<uint8_t> argb(w * h * 4);
                        int result = libyuv::I420ToABGR(yuv_planes[0], sDstBufInfo.UsrData.sSystemBuffer.iStride[0],
                            yuv_planes[1], sDstBufInfo.UsrData.sSystemBuffer.iStride[1],
                            yuv_planes[2], sDstBufInfo.UsrData.sSystemBuffer.iStride[1],
                            argb.data(), w * 4, w, h);
                        stbi_write_png("out.png", video_width, video_height, 4, argb.data(), 0);
                        //output handling (pData[0], pData[1], pData[2])
                        return 0;
                    }
                    printf("frame %04d decoded\n", frame);
                    frame++;
                }

                off += ssz;
            }

            MP4Free(data);

        }

        pSvcDecoder->Uninitialize();
        WelsDestroyDecoder(pSvcDecoder);

        return 0;
    }

    ISVCEncoder* encoder;
    int rv = WelsCreateSVCEncoder(&encoder);

    //SEncParamBase param;
    //memset(&param, 0, sizeof(SEncParamBase));
    //param.iUsageType = EUsageType::CAMERA_VIDEO_REAL_TIME; //from EUsageType enum
    //param.fMaxFrameRate = 25;
    //param.iPicWidth = 400;
    //param.iPicHeight = 400;
    //param.iTargetBitrate = 5000000;
    //param.iRCMode = RC_TIMESTAMP_MODE;
    //encoder->Initialize(&param);

        //Encoder params
    SEncParamExt param;
    encoder->GetDefaultParams(&param);
    param.iUsageType = CAMERA_VIDEO_REAL_TIME;
    param.fMaxFrameRate = 25.f;
    param.iLtrMarkPeriod = 75;
    param.iPicWidth = 1024;
    param.iPicHeight = 512;
    param.iTargetBitrate = 1000 << 10;
    param.bEnableDenoise = false;
    param.iSpatialLayerNum = 1;
    param.bUseLoadBalancing = false;
    param.bEnableSceneChangeDetect = false;
    param.bEnableBackgroundDetection = false;
    param.bEnableAdaptiveQuant = false;
    param.bEnableFrameSkip = false;
    param.iMultipleThreadIdc = 0;
    //param.uiIntraPeriod = 10;

    for (int i = 0; i < param.iSpatialLayerNum; i++)
    {
        param.sSpatialLayers[i].iVideoWidth = param.iPicWidth >> (param.iSpatialLayerNum - 1 - i);
        param.sSpatialLayers[i].iVideoHeight = param.iPicHeight >> (param.iSpatialLayerNum - 1 - i);
        param.sSpatialLayers[i].fFrameRate = 25.f;
        param.sSpatialLayers[i].iSpatialBitrate = param.iTargetBitrate;
        param.sSpatialLayers[i].uiProfileIdc = PRO_BASELINE;
        param.sSpatialLayers[i].uiLevelIdc = LEVEL_4_2;
        param.sSpatialLayers[i].iDLayerQp = 42;

        //SSliceArgument sliceArg;
        //sliceArg.uiSliceMode = SM_FIXEDSLCNUM_SLICE;
        //sliceArg.uiSliceNum = 0; 
        //param.sSpatialLayers[i].sSliceArgument = sliceArg;
    }

    param.uiMaxNalSize = 1500;
    param.iTargetBitrate *= param.iSpatialLayerNum;
    encoder->InitializeExt(&param);


    int trace_level = WELS_LOG_ERROR;
    encoder->SetOption(ENCODER_OPTION_TRACE_LEVEL, &trace_level);
    int videoFormat = videoFormatI420;
    encoder->SetOption(ENCODER_OPTION_DATAFORMAT, &videoFormat);
    
    int frameSize = param.iPicWidth * param.iPicHeight * 3 / 2;
    std::vector<uint8_t> buf(frameSize);

    SFrameBSInfo info;
    memset(&info, 0, sizeof (SFrameBSInfo));
    SSourcePicture pic;
    memset(&pic, 0, sizeof (SSourcePicture));
    pic.iPicWidth = param.iPicWidth;
    pic.iPicHeight = param.iPicHeight;
    pic.iColorFormat = videoFormatI420;
    pic.iStride[0] = pic.iPicWidth;
    pic.iStride[1] = pic.iStride[2] = pic.iPicWidth >> 1;
    pic.pData[0] = buf.data();
    pic.pData[1] = pic.pData[0] + (param.iPicWidth * param.iPicHeight);
    pic.pData[2] = pic.pData[1] + (param.iPicWidth * param.iPicHeight >> 2);

    MP4FileHandle mp4 = MP4Create("out.mp4");
    MP4TrackId mp4_track = -1;
    MP4SetTimeScale(mp4, 90000);

    const int frames = 500;
    //std::ofstream f("out.h264", std::ios::binary);
    for (int num = 0; num < frames; num++)
    {
        printf("encoding %.2f%%\r", (float)num / (float)(frames - 1) * 100.f);

        float value = sinf((float)num / (float)frames * 10.f);
        for (int y = 0; y < param.iPicHeight; y++)
            for (int x = 0; x < param.iPicWidth; x++)
                buf[y * param.iPicWidth + x] = (((y + num / 4) / 10) % 2) * (255.f * value);

        //prepare input data
        rv = encoder->EncodeFrame(&pic, &info);
        //pic.uiTimeStamp += (1.f/25.f) * 1000.f;
        //assert (rv == cmResultSuccess);
        if (info.eFrameType != videoFrameTypeSkip) 
        {
            //output bitstream handling
            for (int layer = 0; layer < info.iLayerNum; layer++)
            {
                size_t bs_size = 0;
                for (int nal = 0; nal < info.sLayerInfo[layer].iNalCount; nal++)
                {
                    std::array<uint8_t, 5> nalu_bytes;
                    for (int i = 0; i < nalu_bytes.size(); i++)
                        nalu_bytes[i] = info.sLayerInfo[layer].pBsBuf[bs_size + i];
                    if (nalu_bytes[4] == 0x67) // SPS
                    {
                        uint8_t avc_profile = info.sLayerInfo[layer].pBsBuf[bs_size + 5];
                        uint8_t avc_profile_compat = info.sLayerInfo[layer].pBsBuf[bs_size + 6];
                        uint8_t avc_level = info.sLayerInfo[layer].pBsBuf[bs_size + 7];
                        if (mp4_track == -1)
                        {
                            mp4_track = MP4AddH264VideoTrack(mp4, 90000, 90000 / 25,
                                param.iPicWidth, param.iPicHeight,
                                avc_profile, avc_profile_compat, avc_level, 3);
                            MP4SetVideoProfileLevel(mp4, 1);
                        }
                        MP4AddH264SequenceParameterSet(mp4, mp4_track, info.sLayerInfo[layer].pBsBuf + bs_size + 4, 
                            info.sLayerInfo[layer].pNalLengthInByte[nal] - 4);
                    }
                    else if (nalu_bytes[4] == 0x68) // PPS
                    {
                        MP4AddH264PictureParameterSet(mp4, mp4_track, info.sLayerInfo[layer].pBsBuf + bs_size + 4,
                            info.sLayerInfo[layer].pNalLengthInByte[nal] - 4);
                    }
                    else
                    {
                        int nalu_sz = info.sLayerInfo[layer].pNalLengthInByte[nal];
                        uint8_t* data = info.sLayerInfo[layer].pBsBuf + bs_size;
                        data[0] = (nalu_sz - 4) >> 24;
                        data[1] = (nalu_sz - 4) >> 16;
                        data[2] = (nalu_sz - 4) >> 8;
                        data[3] = (nalu_sz - 4) & 0xff;
                        bool sync = false;
                        if (nalu_bytes[4] == 0x65) // I-frame
                            sync = true;
                        else // 0x61 P-frame
                            sync = false;
                        MP4WriteSample(mp4, mp4_track, data, nalu_sz, MP4_INVALID_DURATION, 0, sync);
                    }
                    //printf("nalu %x\n", nalu_bytes[4]);
                    bs_size += info.sLayerInfo[layer].pNalLengthInByte[nal];
                }
                //f.write((const char*)info.sLayerInfo[layer].pBsBuf, bs_size);
            }
        }
    }
    //f.close();

    printf("\n");

    MP4Close(mp4);

    if (encoder) 
    {
        encoder->Uninitialize();
        WelsDestroySVCEncoder(encoder);
    }
}
